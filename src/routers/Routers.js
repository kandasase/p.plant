import { createBrowserRouter } from "react-router-dom"
import Layout from '../components/Layout/Layout'
import Home from "../page/Home"
import Shop from '../page/Shop'
import ProductDetails from '../page/ProductDetails'
import Cart from '../page/Cart'
import Checkout from '../page/Checkout'
import Login from '../page/Login'
import Signup from '../page/Signup'
import HowtoGrow from '../page/HowtoGrow'

const routers = createBrowserRouter([
   { path: '/', element: <Layout><Home /></Layout> ,errorElement:<>error</>},
   { path: '/home', element: <Layout><Home /></Layout>,errorElement:<>error</> },
   { path: '/shop', element: <Layout><Shop /></Layout>,errorElement:<>error</> },
   { path: '/shop/:id', element: <Layout><ProductDetails /></Layout>,errorElement:<>error</> },
   { path: '/cart', element: <Layout><Cart /></Layout> },
   { path: '/checkout', element: <Layout><Checkout /></Layout> },
   { path: '/login', element: <Layout><Login /></Layout> },
   { path: '/signup', element: <Layout><Signup /></Layout> },
   { path: '/howto', element: <Layout><HowtoGrow /></Layout> },
]);

export default routers;
