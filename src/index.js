// import './App'
import "./index.css"
import React from 'react';
import * as ReactDOM from 'react-dom/client';
import { ChakraProvider } from '@chakra-ui/react';
import store from './redux/store'
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import App from "./App";
import { ContextProvider } from "./context";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <ChakraProvider>
      <ContextProvider>
    <Provider store={store}>

      <ToastContainer
        theme='colored'
        position='top-right'
        autoClose={2000}
        closeOnClick
        pauseOnHover={false}
        />
      {/* <Layout /> */}
        <App />
      {/* </Layout> */}
    </Provider>
        </ContextProvider>
  </ChakraProvider>
);

