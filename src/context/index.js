import { useContext, useState, createContext } from 'react'

const AppContext = createContext({
  user: null,
  isLoading: true,
})

function ContextProvider({ children }) {
  const [cart, setCart] = useState([])
  const [order, setOrder] = useState([]);
  const { Provider } = AppContext
  const value = {
    cart, setCart, order, setOrder
  }
  return <Provider value={value}>{children}</Provider>
}

const useAppContext = () => useContext(AppContext)

export { ContextProvider, AppContext, useAppContext }