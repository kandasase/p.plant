import { RouterProvider } from 'react-router-dom';
import './App'
import React from 'react';
// import Layout from './components/Layout/Layout';
import routers from './routers/Routers'
function App() {
  return <RouterProvider router={routers} />
}

export default App;