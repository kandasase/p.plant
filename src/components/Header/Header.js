import { Box, Button, Center, Drawer, DrawerBody, DrawerContent, DrawerHeader, DrawerOverlay, Flex, HStack, Icon, Image, Text, VStack, useDisclosure } from '@chakra-ui/react'
import React, { useEffect, useRef } from 'react'
import { motion } from 'framer-motion'
import { TiHeartOutline, TiShoppingBag, TiThList } from "react-icons/ti";
import TextLogo from '../../assets/Text.png'
import Imuser from '../../assets/user.png'
import { useSelector } from 'react-redux';
import { useAppContext } from '../../context';
import { Link } from 'react-router-dom';

const Header = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { cart } = useAppContext()
  const headerRef = useRef(null)
  const totalQuantity = useSelector(state => state.cart.totalQuantity)

  console.log(totalQuantity)
  const stickyHeaderFunc = () => {
    window.addEventListener('scroll', () => {
      if (
        document.body.scrollTop > 80 || document.documentElement.scrollTop > 80
      ) {
        headerRef.current.classList.add('sticky__header')
      } else {
        headerRef.current.classList.remove('sticky__header')
      }
    })
  }
  useEffect(() => {
    stickyHeaderFunc();
    return () => window.removeEventListener('scroll', stickyHeaderFunc)
  })

  return (
    <>
      <Center w="100%" pt="1rem" position="fixed" zIndex={'overlay'} >
        <Center w="90%" pt='1rem' backdropFilter='auto' h='90px' backdropBlur='10px' p="1rem" borderRadius="20px" >
          <Flex justifyContent='space-between'
            alignItems='center'
            bg="white"
            display={{ base: 'none', sm: 'none', md: 'none', lg: 'flex', xl: 'flex', '2xl': 'flex' }}
            padding="0 30px"
            boxShadow='0 5px 20px rgba(0, 0, 0, 0.25)'
            width="100%"
            height="65px"
            borderRadius='1.5rem'>
            <Image src={TextLogo} h='55px' />
            <HStack fontSize={20}>
              <Text as={Link} to='/home' _hover={{ textDecoration: 'none' }}>
                <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                  <Text>Home</Text>
                </HStack>
              </Text>
              <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }}>
                <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                  <Text>Shop</Text>
                </HStack>
              </Text>
              <Text as={Link} to='/cart' _hover={{ textDecoration: 'none' }}>
                <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                  <Text>Cart</Text>
                </HStack>
              </Text>
            </HStack>
            <HStack>
              <Text as={Link} to='/' p='0px' >
                <Icon as={TiHeartOutline} boxSize={8} />
              </Text>
              <Text as={Link} to='/cart' p='0px'>
                <Icon as={TiShoppingBag} boxSize={8} />
                {cart?.length}
              </Text>
              <Text as={Link} to='/login'>
                <motion.img src={Imuser} whileTap={{ scale: 1.2 }} style={{ width: '50px', height: '50px' }} />
              </Text>
            </HStack>
          </Flex>
        </Center>
      </Center>
      {/* size Hamburger */}
      <Center w="100%" pt="1rem" position="fixed" zIndex={'overlay'}>
        <Center w="95%" pt='1rem' p="1rem" borderRadius="20px">
          <Flex
            bg='white'
            display={{ base: 'flex', sm: 'flex', md: 'flex', lg: 'none', xl: 'none', '2xl': 'none' }}
            justifyContent='space-between'
            alignItems='center'
            padding="0 30px"
            boxShadow='0 5px 20px rgba(0, 0, 0, 0.25)'
            width="90%"
            height="65px"
            borderRadius='1.5rem'>
            <Box></Box>
            <Image src={TextLogo} h='40px' />
            <Button color='white' bg='#293462' onClick={onOpen}>
              <TiThList />
            </Button>
            <Drawer placement="top" onClose={onClose} isOpen={isOpen} border='20px'>
              <DrawerOverlay />
              <DrawerContent>
                <DrawerHeader display='flex' w='100%' justifyContent='center' >
                  <Text as={Link} to='/login' _hover={{ textDecoration: 'none' }}>
                    <motion.img src={Imuser} whileTap={{ scale: 1.2 }} style={{ width: '150px', height: '150px' }} />
                  </Text>
                </DrawerHeader>
                <DrawerBody fontSize={20}>
                  <VStack className="nav-menu" >
                    <VStack>
                      <Text as={Link} to='/home' _hover={{ textDecoration: 'none' }}>
                        <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                          <Text>Home</Text>
                        </HStack>
                      </Text>
                      <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }}>
                        <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                          <Text>Shop</Text>
                        </HStack>
                      </Text>
                      <Text as={Link} to='/cart' _hover={{ textDecoration: 'none' }}>
                        <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                          <Text>Cart</Text>
                        </HStack>
                      </Text>
                      <Text as={Link} to='/cart' _hover={{ textDecoration: 'none' }}>
                        <HStack p="0.5rem" _hover={{ color: 'white', bg: '#A0E4CB', borderRadius: "15px" }}>
                          <Icon as={TiShoppingBag} boxSize={8} />
                          <Text >
                            {cart?.length}
                          </Text>
                        </HStack>
                      </Text>
                    </VStack>
                  </VStack>
                </DrawerBody>
              </DrawerContent>
            </Drawer>
          </Flex>
        </Center>
      </Center>
    </>
  )
}

export default Header