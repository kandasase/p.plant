import React from 'react'
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import { Flex } from '@chakra-ui/react'

const Layout = ({children}) => {
   return (
      <>
         <Header />
         <Flex >
            {children}
         </Flex>
         <Footer />
      </>
   )
}

export default Layout