import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Link } from "react-router-dom";
import { Box, Button, Center, Flex, HStack, Image, Text } from "@chakra-ui/react";
import plant1 from '../../assets/plants/01.jpg'
import plant2 from '../../assets/plants/13.jpg'
import plant3 from '../../assets/plants/03.jpg'
export default class SimpleSlider extends Component {
   render() {
      const settings = {
         dots: true,
         infinite: true,
         speed: 500,
         slidesToShow: 1,
         slidesToScroll: 1
      };
      return (
         <Box mb='50px'>
            <Slider {...settings} pb=''>
               <div >
                  <Box textColor='white' h='500px' display={{ base: 'none', lg: 'flex' }} mb='20px'>
                     <Flex w='full' direction={{ base: 'column', lg: 'row', xl: 'row' }} justifyContent='space-around' >
                        <Box
                           p={10}
                           w={{ base: '', lg: '50%' }}
                           direction='column'
                           pb={{ base: '20px', lg: '0px' }}
                        >
                           <Text fontSize='15px' fontWeight='bold' >
                              Trending product in 2023
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px', lg: '40px', xl: '50px' }} >
                              Make your home more natural and modern.
                           </Text>
                           <Box w={{ base: '300px', sm: '350px', md: '400px' }}>
                              Plants are an incredibly important kingdom of organisms.
                              They are multicellular organisms with the amazing ability
                              to make their own food from carbon dioxide in the atmosphere.
                           </Box>
                           <Button mt='10px' borderRadius="20px">
                              <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }}>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text textColor='black'>SHOP NOW</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                        <Center >
                           <Image
                              src={plant3}
                              w={{ base: '95%', md: '70%', lg: '100%' }}
                              h={{ base: '300px', md: '500px', '2xl': '450px' }}
                              borderRadius='15px'
                           />
                        </Center>
                     </Flex>
                  </Box>
                  <Box
                     mb='10px'
                     h='500px'
                     display={{ base: 'flex', lg: 'none' }}
                     style={{ backgroundImage: `url(${plant3})`, backgroundSize: '980px' }}
                  >
                     <Flex direction='column'>
                        <Box p={{ base: 10, md: 20 }} direction='column' pb='20px' >
                           <Text fontSize='15px' fontWeight='bold'>
                              Trending product in 2023
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px' }} >
                              Make your home more natural and modern.
                           </Text>
                           <Button mt='40px' borderRadius="20px">
                              <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }}>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text>SHOP NOW</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                     </Flex>
                  </Box>
               </div>
               <div >
                  <Box h='500px' display={{ base: 'none', lg: 'flex' }} mb='10px' >
                     <Flex textColor='white' w='full' direction='row-reverse' justifyContent='space-around'>
                        <Box
                           p={10}
                           w={{ base: '', lg: '50%' }}
                           direction='column'
                           pb={{ base: '20px', lg: '0px' }}
                        >
                           <Text fontSize='15px' fontWeight='bold' >
                              How to Plants
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px', lg: '40px', xl: '50px' }} >
                              The right way to grow plants
                           </Text>
                           <Box w={{ base: '300px', sm: '350px', md: '400px' }}>
                              Taking care of plants can be very rewarding. No matter how much care and attention you give to plants,
                              they aren't going to thrive if you don't know their specific needs.                           </Box>
                           <Button mt='10px' borderRadius="20px">
                              <Text as={Link} to='/howto' _hover={{ textDecoration: 'none' }}>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text textColor='black'>HOW TO</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                        <Center >
                           <Image
                              src={plant1}
                              w={{ base: '95%', md: '70%', lg: '100%' }}
                              h={{ base: '300px', md: '500px', '2xl': '450px' }}
                              borderRadius='15px'
                           />
                        </Center>
                     </Flex>
                  </Box>
                  <Box
                     mb='10px'
                     h='500px'
                     display={{ base: 'flex', lg: 'none' }}
                     style={{ backgroundImage: `url(${plant1})`, backgroundSize: '980px' }}
                  >
                     <Flex direction='column'>
                        <Box p={{ base: 10, md: 20 }} direction='column' pb='20px' textColor='white'>
                           <Text fontSize='15px' fontWeight='bold'>
                              How to Plants
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px' }}  >
                              The right way to grow plants
                           </Text>
                           <Button mt='40px' borderRadius="20px">
                              <Text as={Link} to='/howto' _hover={{ textDecoration: 'none' }} textColor='black'>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text>HOW TO</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                     </Flex>
                  </Box>
               </div>
               <div >
                  <Box h='500px' display={{ base: 'none', lg: 'flex' }} mb='10px'>
                     <Flex textColor='white' direction='row-reverse' w='full' justifyContent='space-around' >
                        <Box
                           p={10}
                           w={{ base: '', lg: '50%' }}
                           direction='column'
                           pb={{ base: '20px', lg: '0px' }}
                        >
                           <Text fontSize='15px' fontWeight='bold' >
                              Popular in 2023
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px', lg: '40px', xl: '50px' }} >
                              POPULAR CROPS
                           </Text>
                           <Box w={{ base: '300px', sm: '350px', md: '400px' }}>
                              Help create shady and shade for residential buildings.
                              Or planted as an ornamental plant to show the beauty or
                              fragrance that makes the atmosphere more relaxing.                           </Box>
                           <Button mt='10px' borderRadius="20px">
                              <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }}>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text textColor='black'>SHOP NOW</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                        <Center >
                           <Image
                              src={plant2}
                              w={{ base: '95%', md: '70%', lg: '100%' }}
                              h={{ base: '300px', md: '500px', '2xl': '450px' }}
                              borderRadius='15px'
                           />
                        </Center>
                     </Flex>
                  </Box>
                  <Box
                     mb='10px'
                     h='500px'
                     display={{ base: 'flex', lg: 'none' }}
                     style={{ backgroundImage: `url(${plant2})`, backgroundSize: '980px' }}
                  >
                     <Flex direction='column'>
                        <Box p={{ base: 10, md: 20 }} direction='column' pb='20px' textColor='white'>
                           <Text fontSize='15px' fontWeight='bold' >
                              Popular in 2023
                           </Text>
                           <Text fontWeight='bold' fontSize={{ base: '35px', md: '40px' }}  >
                              POPULAR CROPS
                           </Text>
                           <Button mt='40px' borderRadius="20px">
                              <Text as={Link} to='/shop' _hover={{ textDecoration: 'none' }} textColor='black'>
                                 <HStack p="0.5rem" _hover={{ color: 'white', bg: '#3C6255', borderRadius: "15px" }}>
                                    <Text>SHOP NOW</Text>
                                 </HStack>
                              </Text>
                           </Button>
                        </Box>
                     </Flex>
                  </Box>
               </div>
            </Slider>
         </Box>
      );
   }
}