import { Flex, Text } from '@chakra-ui/layout'
import React, { useEffect, useState } from 'react'

const Clock = () => {
   const [days, setDays] = useState()
   const [hours, setHours] = useState()
   const [minutes, setMinutes] = useState()
   const [seconds, setSeconds] = useState()
   let interval;

   const countDown = () => {
      const destination = new Date('June 1, 2023').getTime()
      interval = setInterval(() => {
         const now = new Date().getTime()
         const different = destination - now
         const days = Math.floor(different / (1000 * 60 * 60 * 24))
         const hours = Math.floor(different % (1000 * 60 * 60 * 24) / (1000 * 60 * 60))
         const minutes = Math.floor(different % (1000 * 60 * 60) / (1000 * 60))
         const seconds = Math.floor(different % (1000 * 60) / 1000)

         if (destination < 0) clearInterval(interval.current)
         else {
            setDays(days)
            setHours(hours)
            setMinutes(minutes)
            setSeconds(seconds)
         }
      })
   }
   useEffect(() => {
      countDown()
   })
   return (
      <Flex direction='row' textAlign='center'>
         <Text fontWeight='bold' fontSize={{ base: '10px', md: '40px', lg: '50px' }}>
            {days}
            <Text fontSize={{ base: '10px', md: '15px', lg: '20px' }}>
               Day
            </Text>
         </Text>
         <Text fontWeight='bold' pl='10px' pr='10px' fontSize={{ base: '20px', md: '40px', lg: '50px' }}>:</Text>
         <Text fontWeight='bold' fontSize={{ base: '10px', md: '40px', lg: '50px' }}>
            {hours}
            <Text fontSize={{ base: '10px', md: '15px', lg: '20px' }}>
               Hours
            </Text>
         </Text>
         <Text fontWeight='bold' pl='10px' pr='10px' fontSize={{ base: '20px', md: '40px', lg: '50px' }}>:</Text>
         <Text fontWeight='bold' fontSize={{ base: '10px', md: '40px', lg: '50px' }}>
            {minutes}
            <Text fontSize={{ base: '10px', md: '15px', lg: '20px' }}>
               Minutes
            </Text>
         </Text>
         <Text fontWeight='bold' pl='10px' pr='10px' fontSize={{ base: '20px', md: '40px', lg: '50px' }}>:</Text>
         <Text fontWeight='bold' fontSize={{ base: '10px', md: '40px', lg: '50px' }}>
            {seconds}
            <Text fontSize={{ base: '10px', md: '15px', lg: '20px' }}>
               Seconds
            </Text>
         </Text>
      </Flex>
   )
}

export default Clock