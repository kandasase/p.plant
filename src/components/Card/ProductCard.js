import { Badge, Box, Button, Flex, HStack, Image, Text, chakra, shouldForwardProp } from '@chakra-ui/react'
import React from 'react'
import { isValidMotionProp, motion } from 'framer-motion';
import { toast } from 'react-toastify';
import { Link, useNavigate } from 'react-router-dom';
import { useAppContext } from '../../context';
import { FaCloudSun, FaSun } from "react-icons/fa";

const ProductCard = ({ item }) => {
   const { setCart, cart } = useAppContext()
   const ChakraBox = chakra(motion.div, {
      shouldForwardProp: (prop) => isValidMotionProp(prop) || shouldForwardProp(prop),
   });
   const addToCart = () => {
      setCart([...cart, {
         id: item.id,
         productName: item.productName,
         price: item.price,
         img: item.img,
         amount: 1
      },])
      toast.success('product added to the cart')
   };
   const navigate = useNavigate();

   function color() {
      const colorShow = item.category
      if (colorShow === 'garden') {
         return 'green'
      }
      if (colorShow === 'flower') {
         return 'red'
      }
      if (colorShow === 'ivy') {
         return 'yellow'
      }
   }
   function condition() {
      const conditionShow = item.milieu
      if (conditionShow === 'sunshine') {
         return <FaSun />
      }
      if (conditionShow === 'partial sun, shade') {
         return <FaCloudSun />
      }
   }
   console.log(item)
   return (
      <Flex p='10px'>
         <Box maxW='sm' h='390px' w='200px' borderBottomWidth='2px' borderRadius='20px' overflow='hidden'>
            <ChakraBox
               _hover={{
                  cursor: 'pointer'
               }}
               whileHover={{ scale: 2.1 }}
               onClick={() => {
                  navigate(`/shop/${item.id}`);
               }}
            >
               <Image src={item.img} h='250px' w='200px' objectFit='cover' />
            </ChakraBox>
            <Box p='6'>
               <Box fontWeight='bold' >
                  <Text as={Link} to={`/shop/${item.id}`}
                     // href='/shop/:id'
                     _hover={{
                        textDecoration: 'none',
                        color: 'white',
                        bg: '#1C315E',
                        borderRadius: "5px"
                     }}
                  >
                     {item.productName}
                  </Text>
               </Box>
               <Box display='flex' alignItems='baseline'>
                  <Badge borderRadius='full' colorScheme={color()} fontSize={10}>
                     {item.category}
                  </Badge>
                  <Box
                     color='gray.500'
                     fontWeight='semibold'
                     letterSpacing='wide'
                     fontSize='xs'
                     textTransform='uppercase'
                     ml='5px'
                  >
                     {condition()}
                  </Box>
               </Box>
               <HStack justifyContent='space-between' >
                  <Box pt='2px' textAlign='end'>
                     ฿{item.price}
                  </Box>
                  <Button borderRadius='20px' onClick={addToCart}>
                     +
                  </Button>
               </HStack>
            </Box>
         </Box>
      </Flex>
   )
}

export default ProductCard