import React from 'react'
import ProductCard from './ProductCard'
import { Grid, GridItem } from '@chakra-ui/react'

const ProductsList = ({ data, }) => {
   return (
      <>
         <Grid justifyItems='center' pb='20px' templateColumns={{ base: 'repeat(1, 1fr)', sm: 'repeat(2, 1fr)', md: 'repeat(3, 1fr)', lg: 'repeat(4, 1fr)' }} gap={6}>
            {data?.map((item, index) => (
               <GridItem>
                  <ProductCard item={item} key={index} />
               </GridItem>
            ))}
         </Grid>
      </>
   )
}

export default ProductsList