import { Box, Flex } from '@chakra-ui/react'
import React from 'react'
import '../../styles/common-section.css'

const CommonSection = ({ title }) => {
  return (
    <Box className='common__section'>
      <Flex> 
        {title}
      </Flex>
    </Box>
  )
}

export default CommonSection

//ถ้าเอา css 21.30