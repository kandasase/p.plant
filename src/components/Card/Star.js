import { Box, Icon } from '@chakra-ui/react'
import React from 'react'
import { BsStar, BsStarFill, BsStarHalf } from 'react-icons/bs'

export default function Star({ avgRating }) {
  switch (avgRating) {
    case 1:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
    case 1.5:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarHalf} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
    case 2:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
    case 2.5:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarHalf} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
    case 3:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
    case 3.5:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarHalf} />
          <Icon as={BsStar} />
        </Box>
      )
    case 4:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStar} />
        </Box>
      )
    case 4.5:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarHalf} />
        </Box>
      )
    case 5:
      return (
        <Box>
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
          <Icon as={BsStarFill} />
        </Box>
      )
    default:
      return (
        <Box>
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
          <Icon as={BsStar} />
        </Box>
      )
  }
}
  //แบบเก่า
// {<span>
// {avgRating === 5 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//   </Box>
// ) : avgRating === 4.5 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarHalf} />
//   </Box>
// ) : avgRating === 4 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 3.5 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarHalf} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 3 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 2.5 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarHalf} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 2 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarFill} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 1.5 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStarHalf} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// ) : avgRating === 1 ? (
//   <Box>
//     <Icon as={BsStarFill} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// ) : (
//   <Box>
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//     <Icon as={BsStar} />
//   </Box>
// )}
// </span> }
