import { Box } from "@chakra-ui/react";
import React from "react";
const Helmet = (props) => {
   document.title = 'Maltimart -' + props.title;
   return <Box>{props.children}</Box>
}
export default Helmet;