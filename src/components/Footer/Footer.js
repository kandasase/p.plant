import { Box, Center, Divider, Flex, Icon, Text } from '@chakra-ui/react'
import { Link } from 'react-router-dom'
import React from 'react'
import { FaMailBulk, FaMapMarkerAlt, FaPhone, FaArrowRight } from 'react-icons/fa'

const Footer = () => {
  return (
    <Box bgColor='#235d3a'>
      <Flex textColor='white' w='auto' h='auto' p='30px' direction={{ base: 'column', lg: 'row' }} justifyContent='space-around'>
        <Box p='10px'>
          <Text fontWeight='bold' pb='20px' fontSize='18px' >
            Plants
          </Text>
          <Text w={{ base: '200px', md: '400px' }}>
            Our plants are planted by ourselves, paying attention to every plant,
            from seed to delivery to your hands.

          </Text>
        </Box>

        <Box p='10px' fontSize='13px'>
          <Text fontWeight='bold' pb='20px' fontSize='18px'>Top Categories</Text>
          <Text as={Link} to='/shop'>
            <Icon as={FaArrowRight} mr="13px" />
            Garden Tree
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/shop'>
              <Icon as={FaArrowRight} mr="13px" />
              Flower
            </Text>
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/shop'>
              <Icon as={FaArrowRight} mr="13px" />
              Ivy
            </Text>
          </Text>
        </Box>
        <Box p='10px' fontSize='13px'>
          <Text fontWeight='bold' pb='20px' fontSize='18px'>Useful Links</Text>
          <Text as={Link} to='/home'>
            <Icon as={FaArrowRight} mr="13px" />
            Home
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/shop'>
              <Icon as={FaArrowRight} mr="13px" />
              Shop
            </Text>
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/cart'>
              <Icon as={FaArrowRight} mr="13px" />
              Cart
            </Text>
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/login'>
              <Icon as={FaArrowRight} mr="13px" />
              Login
            </Text>
          </Text>
          <Text pt='10px'>
            <Text as={Link} to='/howto'>
              <Icon as={FaArrowRight} mr="13px" />
              How to
            </Text>
          </Text>
        </Box>
        <Box p='10px' fontSize='13px'>
          <Text fontWeight='bold' pb='20px' fontSize='18px'>Contact</Text>
          <Text as={Link} to='https://goo.gl/maps/C9mNDb7Typ2eXdXv6'>
            <Icon as={FaMapMarkerAlt} mr="13px" />
            University of Phayao
          </Text>
          <Text pt='10px'>
            <Icon as={FaPhone} mr="13px" />
            +0635163145
          </Text>
          <Text pt='10px'>
            <Icon as={FaMailBulk} mr="13px" />
            warinthan.j@gmail.com
          </Text>
        </Box>
      </Flex>
      <Box>
        <Center>
          <Divider pb='5px' color='gray' w='80%' />
        </Center>
        <Text textColor='white' p='30px' pt='10px' textAlign='center'>
          © The PLANTS 2023 developed by Warinthan Junjarin. All Rights Reserved.
        </Text>
      </Box>
    </Box>

  )
}

export default Footer