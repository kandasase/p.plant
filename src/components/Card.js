import { Box, useStyleConfig } from '@chakra-ui/react';

function CardContent(props) {
  const { variant, children, ...rest } = props;
  const styles = useStyleConfig('Card', { variant });
  return (
    <Box
      boxShadow="0px 0px 10px rgba(0, 0, 0, 0.1)"
      p={{
        base: '20px 10px 20px 10px',
        sm: '40px 50px 40px 50px',
        md: '50px 60px 50px 60px',
        lg: '0px 0x 0px 0px',
        xl: '60px 100px 60px 100px',
      }}
      bg={{
        base: '#2B3A55',
        sm: '#2B3A55',
        xl: '#2B3A55',
        md: '#2B3A55',
        lg: '#2B3A55',
      }}
      borderRadius="30px"
      // w={{ base: '75%', sm: '80%', md: '60%', lg: '40%' }}
      w="10rem"
      {...rest}
      __css={styles}
    >
      {children}
    </Box>
  );
}
export { CardContent };
