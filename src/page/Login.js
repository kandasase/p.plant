import {
  Box,
  Button,
  Center,
  Divider,
  FormControl,
  FormErrorMessage,
  HStack,
  Input,
  Text,
  VStack,
} from '@chakra-ui/react';
import { Controller, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { CardContent } from '../components/Card';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { auth } from '../config';
import { useToast } from '@chakra-ui/react';

const Login = () => {
  const toast = useToast();
  const navigate = useNavigate();
  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();
  function onSubmit(data) {
    const { email, password } = data;
    signInWithEmailAndPassword(auth, email, password)
      .then(() => {
        navigate('/');
        toast({
          title: `
          Login`,
          position: 'top',
          isClosable: true,
          duration: 3000,
          status: 'success',
        });
      })
      .catch(() => {
        toast({
          title: `Error`,
          position: 'top',
          isClosable: false,
          duration: 3000,
          status: 'error',
        });
      });
  }
  return (
    <Center mt={{ base: '12rem', sm: '15rem' }} w="100%" mb="10rem">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box>
          <CardContent w={{ sm: '100%', md: '100%' }}>
            <HStack>
              <VStack>
                <Text fontWeight="bold" fontSize={{ md: '18px' }} color="white">
                  Sign In
                </Text>
                <Controller
                  name="email"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: {
                      value: true,
                      message: 'Please enter your email.',
                    },
                  }}
                  render={({ field: { onChange, onBlur, value, name } }) => (
                    <FormControl pt="20px" isInvalid={errors[name]}>
                      <Input
                        type="email"
                        placeholder="Please enter your email"
                        bg="#F1F1F1"
                        h={{ base: '4.5vh', md: '5vh' }}
                        w={{ base: '20rem' }}
                        fontSize={{ base: '13px' }}
                        value={value}
                        onBlur={onBlur}
                        onChange={onChange}
                      />
                      <FormErrorMessage>
                        {errors[name] && errors[name]?.message}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                />
                <Controller
                  name="password"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: {
                      value: true,
                      message: 'Please enter your password.',
                    },
                  }}
                  render={({ field: { onChange, onBlur, value, name } }) => (
                    <FormControl pb={{ base: '20px' }} isInvalid={errors[name]}>
                      <Input
                        type="password"
                        placeholder="Please enter your password"
                        bg="#F1F1F1"
                        h={{ base: '4.5vh', md: '5vh' }}
                        w={{ base: '20rem' }}
                        fontSize={{ base: '13px' }}
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                      />
                      <FormErrorMessage>
                        {errors[name] && errors[name]?.message}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                />

                <Button
                  bg="#FFFBEB"
                  w={{ base: '50%' }}
                  color="black"
                  fontSize={{ base: '14px' }}
                  type="submit"
                >
                  Login
                </Button>
                <Divider pt={{ md: '10px' }} />
                <Button
                  bg="#FFFBEB"
                  w={{ base: '50%' }}
                  color="black"
                  fontSize={{ base: '14px' }}
                  onClick={() => {
                    navigate('/signup');
                  }}
                >
                  Register
                </Button>
              </VStack>
            </HStack>
          </CardContent>
        </Box>
      </form>
    </Center>
  );
};

export default Login;
