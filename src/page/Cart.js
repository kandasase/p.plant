import { Box, Button, Center, Image, Table, TableCaption, SimpleGrid, HStack, TableContainer, Tbody, Td, Text, Th, Thead, Tr } from '@chakra-ui/react'
import React, { useCallback, useState, async, useEffect } from 'react'
import Imgproduce from '../assets/plants/16.jpg'
import { cartActions } from '../redux/slices/cartSlice'
import { useSelector, useDispatch } from 'react-redux'
import { RiDeleteBinFill } from 'react-icons/ri'
import { useAppContext } from '../context'
import products from '../mock/products.json'

//เริ่มแก้

const Cart = () => {
  const { cart } = useAppContext()
  // const cartItems = useSelector((state) => state.cart.cartItems)
  const [product, setProduct] = useState([])
  const { order, setOrder } = useState()
  const fetchData = useCallback(async () => {
    setProduct(products)
  }, [])

  useEffect(() => {
    fetchData()
  }, [fetchData])

  const handleAddItem = async (itemClick) => {
    setOrder((prev) => {
      const isItemInCart = prev.find((item) => item.id === itemClick.id);
      if (isItemInCart) {
        return prev.map((item) =>
          item.id === itemClick.id ? { ...item, amount: item.amount + 1 } : item
        );
      }
      return [...prev, { ...itemClick, amount: 1 }];
    });
  };
  const handleRemove = async (id) => {
    setOrder((prev) =>
      prev.reduce((value, item) => {
        if (item.id === id) {
          if (item.amount === 1) {
            return value;
          }
          return [...value, { ...item, amount: item.amount - 1 }];
        } else {
          return [...value, item];
        }
      }, [])
    );
  };
  const handleRemoveItem = async (id) => {
    setOrder((prev) =>
      prev.reduce((value, item) => {
        if (item.id === id) {
          if (item.amount === item.amount) {
            // eslint-disable-next-line no-self-compare
            return value;
          }
          return [...value, { ...item, amount: item.amount - item.amount }];
        } else {
          return [...value, item];
        }
      }, [])
    );
  };


  return (
    <Box w='100%' >
      <Box
        backgroundImage={Imgproduce}
        backgroundPosition='center'
        borderBottomRadius='40px'
        h='400px'
        w='100%'
      >
        <Text
          fontWeight='bold'
          fontSize='50px'
          textAlign='center'
          pt='180px'
          color='white'
        >
          Shopping Cart
        </Text>
      </Box>
      <Box w='100%' pt='80px' pb='50px' h='auto' pl='50px' pr='50px'>
        {
          cart.length === 0 ?
            (<Text
              fontWeight='bold'
              fontSize='30px'
              textAlign='center'
              pt='3rem'
              pb='3rem'>
              No item added to the cart
            </Text>
            )
            :
            (<TableContainer justifyContent='center' overflow='none'>
              <Table variant='simple'>
                <Thead>
                  <Tr >
                    <Th>Image</Th>
                    <Th >Title</Th>
                    <Th >Price</Th>
                    <Th>Qty</Th>
                    <Th whileTap={{ scale: 1.2 }}>Delete</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {
                    cart.map((item, index) => (
                      <Tr>
                        <Td w='100%'>
                          <Image src={item.img} alt='' h="200px" w='150px' />
                        </Td>
                        <Td>
                          {item.productName}
                        </Td>
                        <Td>
                          ฿{item.price}
                        </Td>
                        <Td>
                          {item.amount}
                        </Td>
                        <Td>
                          <RiDeleteBinFill color='red' />
                        </Td>
                      </Tr>
                    ))
                  }
                </Tbody>
              </Table>
            </TableContainer>
            )
        }
      </Box>
    </Box>
  )
}

export default Cart