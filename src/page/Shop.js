import React, { useState } from 'react'
import CommonSection from '../components/Card/CommonSection'
import Helmet from '../components/Helmet/Helmet'
import { Box, Button, HStack, InputGroup, InputLeftAddon, InputRightElement, Stack, Text } from '@chakra-ui/react'
import Imgproduce from '../assets/plants/16.jpg'
import { Select } from '@chakra-ui/react'
import { Input } from '@chakra-ui/react'
import { FiSearch } from 'react-icons/fi'
import products from '../mock/products.json'
import ProductsList from '../components/Card/ProductsList'
import { getDocs, collection } from 'firebase/firestore';

const Shop = () => {
  const [productsData, setProductsData] = useState(products)
  // const fetchData = useCallback
  const handleFilter = (e) => {
    const filterValue = e.target.value
    if (filterValue === 'ALL') {
      const filterProducts = products.filter(
        (item) => item.category
      )
      setProductsData(filterProducts)
    }
    if (filterValue === 'GARDEN TREE') {
      const filterProducts = products.filter(
        (item) => item.category === 'GARDEN TREE'
      )
      setProductsData(filterProducts)
    }
    if (filterValue === 'FLOWER') {
      const filterProducts = products.filter(
        (item) => item.category === 'FLOWER'
      )
      setProductsData(filterProducts)
    }
    if (filterValue === 'IVY') {
      const filterProducts = products.filter(
        (item) => item.category === 'IVY'
      )
      setProductsData(filterProducts)
    }
    // if (filterValue === 'ascending') {
    //   const filterProducts = products.sort((p1,p2)=>
    //   p1.price.localeCompare(p2.price, undefined, {numeric: true})
    //   )
    //   setProductsData([...filterProducts])
    // }

  }
  console.log(productsData)
  const handleAsDes = (e) => {
    const filterasdes = e.target.value
    if (filterasdes === "ascending") {
      // const desProducts = products.sort((el1, el2) =>
      //   el1.price.localeCompare(el2.price, undefined, { numeric: true })
      // );
      const results = productsData.sort((a, b) => a.price - b.price)
      setProductsData(results);
    }
    if (filterasdes === "descending") {
      // const desProducts = products.sort((el1, el2) =>
      //   el2.price.localeCompare(el1.price, undefined, { numeric: true })
      // );
      const results = productsData.sort((a, b) => b.price - a.price)
      setProductsData(results);
    }
  }
  const handleSearch = e => {
    const searchTerm = e.target.value
    const searchedProducts = products.filter(item => item.productName.
      toLowerCase().includes(searchTerm.toLowerCase()))
    setProductsData(searchedProducts)
  }
  return (
    // <Helmet title='Shop'>
    //   <CommonSection title='Products'/>
    // </Helmet>
    <Box w='100%'>
      <Box
        backgroundImage={Imgproduce}
        backgroundPosition='center'
        borderBottomRadius='40px'
        h='400px'
        w='100%'
      >
        <Text
          fontWeight='bold'
          fontSize='50px'
          textAlign='center'
          pt='180px'
          color='white'
        >
          Products
        </Text>
      </Box>
      <Box w='100%' pt='50px' pb='50px' h='auto'>
        <HStack justifyContent='space-around' >
          <Select
            placeholder='Category'
            w='auto'
            bg='#E8F3D6'
            onChange={handleFilter}
          >
            <option value='GARDEN TREE'>Garden tree</option>
            <option value='FLOWER'>Flower</option>
            <option value='IVY'>Ivy</option>
            <option value='ALL'>All</option>
          </Select>
          <Select
            placeholder='Sort By'
            w='auto'
            bg='#E8F3D6'
            onChange={handleAsDes}
          >
            <option value='ascending'>Ascending</option>
            <option value='descending'>Descending</option>
          </Select>
          <Stack spacing={4}>
            <InputGroup>
              <InputLeftAddon children={<FiSearch boxSize={60} />} bg='#E8F3D6' />
              <Input
                bg='white'
                placeholder='Search...'
                onChange={handleSearch}
              />
            </InputGroup>
          </Stack>
        </HStack>
        <Box pt='50px'>
          {productsData.length === 0 ? (
            <Text
              fontWeight='bold'
              fontSize='30px'
              textAlign='center'
              pt='3rem'
              pb='3rem'
            >
              No products are found!
            </Text>
          ) : (
            <ProductsList data={productsData} />
          )}
        </Box>
      </Box>
    </Box>
  )
}
export default Shop
//25.30

//search https://chakra-ui.com/docs/components/input/usage