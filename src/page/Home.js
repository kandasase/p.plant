import React, { useState, useEffect, useCallback } from 'react'
import { Box, Button, Flex, Grid, Image, Img, Text, chakra, shouldForwardProp } from '@chakra-ui/react'
import { motion, isValidMotionProp } from 'framer-motion';
import { useNavigate } from 'react-router-dom'
import SimpleSlider from '../components/Layout/SimpleSlider'
import ico1 from '../assets/comment/12.png'
import ico2 from '../assets/comment/09.png'
import ico3 from '../assets/comment/10.png'
import ico4 from '../assets/comment/11.png'
// import newSticker from '../assets/new.png'
import ProductsList from '../components/Card/ProductsList';
import imageTime from '../assets/product/41.png'
import Clock from '../components/Card/Clock';
import { db } from '../config/index'
import { getDocs, collection } from 'firebase/firestore';

const Home = () => {
  const [trendingProducts, setTrendingProducts] = useState([]);
  const [baseSalesProducts, setBaseSalesProducts] = useState([]);
  const [newProducts, setNewProducts] = useState([]);
  const ChakraBox = chakra(motion.div, {
    shouldForwardProp: (prop) => isValidMotionProp(prop) || shouldForwardProp(prop),
  });

  const fetchData = useCallback(
    async () => {
      const ref = collection(db, "Plants");
      const snapshot = await (await getDocs(ref)).docs.map(doc => ({ id: doc.id, ...doc.data() }))

      const filteredTrendingProducts = snapshot.filter(
        (item, index) => item.category === 'garden'
        // (item,index) => item.category === 'GARDEN TREE' && index === 2 

      );
      const filteredBaseSalesProducts = snapshot.filter(
        (item) => item.category === 'ivy'
      );
      const filteredNewProducts = snapshot.sort(
        (a, b) => new Date(b.createdAt.toDate()) - new Date(a.createdAt.toDate())
      ).filter((item, index) => index < 4)
      setTrendingProducts(filteredTrendingProducts);
      setBaseSalesProducts(filteredBaseSalesProducts);
      setNewProducts(filteredNewProducts);

    },
    [],
  )


  useEffect(() => {
    fetchData()
  }, []);

  const navigate = useNavigate();
  return (
    <Box pt='15vh' bg='#397D54' w='100%' h='100%' justifyContent="space-evenly">
      <Box ml={{ base: '', '2xl': '80px' }} mr={{ base: '', '2xl': '80px' }} >
        <SimpleSlider />
      </Box>
      <Box pt='20px' pb='60px' bg='#73c088'>
        <Grid justifyItems='center' mt='3rem' p='20px' templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(2, 1fr)', lg: 'repeat(4, 1fr)' }} gap={10} >
          <ChakraBox whileHover={{ scale: 1.05 }} h='auto' bg='#F7F7F7' p='10px' w='auto' borderRadius='20px'>
            <Flex direction='row'>
              <Image src={ico1} h='5rem' />
              <Text p={4} fontWeight='bold'>
                FREE SHOPPING
                <Text fontWeight='light' >
                  Free shopping Choose to buy a variety of plants.
                </Text>
              </Text>
            </Flex>
          </ChakraBox>
          <ChakraBox whileHover={{ scale: 1.05 }} h='auto' bg='#F7F7F7' p='10px' w='auto' borderRadius='20px'>
            <Flex direction='row'>
              <Image src={ico2} h='5rem' />
              <Text p={4} fontWeight='bold'>
                VARIOUS PLANTS
                <Text fontWeight='light' >
                  There are many types and types to choose from for shopping.
                </Text>
              </Text>
            </Flex>
          </ChakraBox>
          <ChakraBox whileHover={{ scale: 1.05 }} h='auto' bg='#F7F7F7' p='10px' w='auto' borderRadius='20px'>
            <Flex direction='row'>
              <Image src={ico3} h='5rem' />
              <Text p={4} fontWeight='bold'>
                TRANSPORTATION
                <Text fontWeight='light' >
                  Free shopping Choose to buy a variety of plants.
                </Text>
              </Text>
            </Flex>
          </ChakraBox>
          <ChakraBox whileHover={{ scale: 1.05 }} h='auto' bg='#F7F7F7' p='10px' w='auto' borderRadius='20px'>
            <Flex direction='row'>
              <Image src={ico4} h='5rem' />
              <Text p={4} fontWeight='bold'>
                SERVICE WITH HEART
                <Text fontWeight='light' >
                  Serve customers with heart and intention for the benefit of customers.              </Text>
              </Text>
            </Flex>
          </ChakraBox>
        </Grid>
      </Box>
      <Box bg='white' >
        <Text
          pt='60px'
          pb='40px'
          fontWeight='bold'
          fontSize='32px'
          textAlign='center'
        >
          Trending Products
        </Text>
        <ProductsList data={trendingProducts} />
        <Text
          pt='60px'
          pb='40px'
          fontWeight='bold'
          fontSize='32px'
          textAlign='center'
        >
          Best Sales
        </Text>
        <ProductsList data={baseSalesProducts} />
      </Box>
      <Box bg='#143F17' p='30px' mt='0px' borderTopRightRadius='40px'>
        <Flex direction='row' justifyContent='space-around'>
          <Text mt='40px' p={{ base: '10px', md: '30px', lg: '40px' }} color='white'>
            <Text fontSize={{ base: '10px', md: '15px' }}>
              Limited Offers
            </Text>
            <Text pb='25px' fontSize={{ base: '20px', md: '30px', lg: '40px' }}>
              Homalomena
            </Text>
            <Clock />
            <Button
              p='5px' borderRadius='10px' w='200px'
              mt='30px' fontSize='13px'
              textColor='black'
              onClick={() => {
                navigate('/shop');
              }}
            >
              Visit Store
            </Button>
          </Text>
          <Image src={imageTime} h={{ base: '25vh', md: '40vh', lg: '50vh' }} />
        </Flex>
      </Box>
      <Box bg='white' >
        <Text
          pt='60px'
          pb='40px'
          fontWeight='bold'
          fontSize='32px'
          textAlign='center'
        >
          New Arrivals
        </Text>
        <Box>
          {/* <Img src={newSticker} h={20} position="absolute" zIndex={'overlay'} /> */}
          <ProductsList data={newProducts} />
        </Box>
      </Box>
    </Box>
  )
}

export default Home