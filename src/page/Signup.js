import {
  Box,
  Button,
  Center,
  FormControl,
  FormErrorMessage,
  HStack,
  Input,
  Text,
  VStack,
} from '@chakra-ui/react';
import { Controller, useForm } from 'react-hook-form';
import { CardContent } from '../components/Card';
import { createUserWithEmailAndPassword } from 'firebase/auth';
import { auth, db } from '../config';
import { doc, setDoc } from 'firebase/firestore';
import { useToast } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';

const Signup = () => {
  const navigate = useNavigate();
  const toast = useToast();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  function onSubmit(data) {
    const { email, password } = data;
    return createUserWithEmailAndPassword(auth, email, password)
      .then(res => {
        const { user } = res;
        const { uid } = user;
        const docRef = doc(db, 'Profiles', uid);
        return setDoc(docRef, { email: email });
      })
      .then(() => {
        navigate('/login');
        toast({
          title: `Successful account registration`,
          position: 'top',
          isClosable: true,
          duration: 3000,
          status: 'success',
        });
      });
  }

  return (
    <Center mt="20rem" w="100%" mb="10rem">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box>
          <CardContent w={{ sm: '100%', md: '100%' }}>
            <HStack>
              <VStack>
                <Text fontWeight="bold" fontSize={{ md: '18px' }} color="white">
                  Register
                </Text>

                <Controller
                  name="email"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: {
                      value: true,
                      message: 'Please enter your email.',
                    },
                  }}
                  render={({ field: { onChange, onBlur, value, name } }) => (
                    <FormControl pt="20px">
                      <Input
                        type="email"
                        placeholder="Please enter your email"
                        bg="#F1F1F1"
                        h={{ base: '4.5vh', md: '5vh' }}
                        w={{ base: '20rem' }}
                        fontSize={{ base: '13px' }}
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                      />
                      <FormErrorMessage>
                        {errors[name] && errors[name]?.message}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                />
                <Controller
                  name="password"
                  control={control}
                  defaultValue=""
                  rules={{
                    required: {
                      value: true,
                      message: 'Please enter your password.',
                    },
                  }}
                  render={({ field: { onChange, onBlur, value, name } }) => (
                    <FormControl pb={{ base: '20px' }}>
                      <Input
                        type="password"
                        placeholder="Please enter your password"
                        bg="#F1F1F1"
                        h={{ base: '4.5vh', md: '5vh' }}
                        w={{ base: '20rem' }}
                        fontSize={{ base: '13px' }}
                        value={value}
                        onChange={onChange}
                        onBlur={onBlur}
                      />
                      <FormErrorMessage>
                        {errors[name] && errors[name]?.message}
                      </FormErrorMessage>
                    </FormControl>
                  )}
                />
                <Button
                  bg="#FFFBEB"
                  w={{ base: '50%' }}
                  color="black"
                  fontSize={{ base: '14px' }}
                  type="submit"
                >
                  Create an account
                </Button>
              </VStack>
            </HStack>
          </CardContent>
        </Box>{' '}
      </form>
    </Center>
  );
};

export default Signup;
