import React, { useState, useRef, useEffect } from 'react'
import products from '../mock/products.json'
// import CommonSection from '../components/UI/CommonSection'
import { Box, Button, Divider, Flex, HStack, Icon, Img, Input, Radio, RadioGroup, Stack, Text, Textarea } from '@chakra-ui/react'
import { Tabs, TabList, TabPanels, Tab, TabPanel } from '@chakra-ui/react'
import { Form, Link, useParams } from 'react-router-dom'
import Imgproduce from '../assets/plants/16.jpg'
import ProductsList from '../components/Card/ProductsList'
import { BsStarFill } from 'react-icons/bs';
import { toast } from 'react-toastify'
import Star from '../components/Card/Star'
import { useAppContext } from '../context'
import { db } from '../config/index'
import { getDoc, doc } from 'firebase/firestore';
import { FaArrowLeft } from 'react-icons/fa';

const ProductDetails = () => {
  // const dispatch = useDispatch()
  const [rating, setRating] = useState(null)
  const reviewUser = useRef('')
  const reviewMsg = useRef('')
  const { id } = useParams()
  const [product, setProduct] = useState()
  const { setCart, cart } = useAppContext()

  const relatedProducts = products.filter(item => item.category === product?.category)
  const submitHandler = (e) => {
    e.preventDefault()
    const reviewUserName = reviewUser.current.value
    const reviewUserMsg = reviewMsg.current.value
    const reviewObj = {
      userName: reviewUserName,
      text: reviewUserMsg,
      rating,
    }
    console.log(reviewObj)
    toast.success('Review success')
  }

  const addToCart = () => {
    // dispatch(
    //   cartActions.addItem({
    //     id,
    //     img: img,
    //     productName,
    //     price,
    //   })
    // )
    setCart([...cart, {
      id,
      img: product?.img,
      productName: product?.productName,
      price: product?.price,
    }])
    toast.success('Product added successfuly to Cart')
  }


  const fetchData = async () => {
    const docRef = doc(db, "Plants", id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      setProduct(docSnap.data());
    } else {
      // doc.data() will be undefined in this case
      console.log("No such document!");
    }
  }
  useEffect(() => {
    window.scrollTo(0, 0)
    fetchData()
  }, [id])

  return (
    <Box w='100%'>
      <Box
        backgroundImage={Imgproduce}
        backgroundPosition='center'
        borderBottomRadius='40px'
        h='400px'
        w='100%'
      >
        <Text
          fontWeight='bold'
          fontSize='50px'
          textAlign='center'
          pt='180px'
          color='white'
        >
          {product?.productName}
        </Text>
      </Box>
      {/* <CommonSection /> */}
      <Box w='100%' pt='80px' pb='50px' h='auto' >
        <Flex flexDirection={{ base: 'column', md: 'row' }} justifyContent='space-evenly'>
          <Flex direction='column'>
            <Text as={Link} to='/shop' fontWeight='bold' mb='20px' _hover={{ textDecoration: 'none' }}>
              <Icon as={FaArrowLeft} mr="15px" />
              Back
            </Text>
            <Img src={product?.img} h='30rem' borderRadius='20px' />
          </Flex>
          <Box flexDirection='column' p={{ base: '20px', md: ' ' }}>
            <Text fontWeight='bold' fontSize='30px' pb={{ base: '10px', md: '80px' }} pt={{ base: '30px', md: '0px' }} textAlign='center'>
              {product?.productName}
            </Text>
            Category : {product?.category}
            <Flex alignItems='center' W='50%'>
              Environment : {product?.milieu}
              <Text pl='5px'>
                {product?.condition}
              </Text>
            </Flex>
            <HStack color='orange.400'>
              <Star avgRating={product?.avgRating} />
              <Text>( {product?.avgRating} ratings )</Text>
            </HStack>
            <Text fontWeight='bold' pt='20px' fontSize='20px'>
              ฿ {product?.price}
            </Text>
            <Box w='80%'>
              <Text >
                {product?.shortDesc}
              </Text>
            </Box>
            <Button
              mt='40px'
              colorScheme='green'
              _hover={{
                transform: 'scale(1.07)'
              }}
              onClick={addToCart}
            >
              Add to Cart
            </Button>
          </Box>
        </Flex>
        <Box alignSelf='center'>
          <Divider pt='40px' pb='5px' color='gray' w='90%' />
        </Box>
        <Flex pt='15px' pl={{ base: '15px', lg: '50px' }} >
          <Tabs variant='soft-rounded' colorScheme='green'>
            <TabList>
              <Tab>Description</Tab>
              <Tab>reviews ({product?.reviews.length})</Tab>
            </TabList>
            <TabPanels>
              <TabPanel>
                <Text w='80%'>
                  {product?.description}
                </Text>
              </TabPanel>
              <TabPanel >
                <Text>
                  <ul>
                    {
                      product?.reviews?.map((item, index) => (
                        <Text kew={index} pb='20px'>
                          <Text fontWeight='bold'>{item.nameReview}</Text>
                          <Text fontSize='13px' textColor='orange.400'>
                            {item.rating} (rating)
                          </Text>
                          <Text>{item.text}</Text>
                        </Text>
                      ))
                    }
                  </ul>
                </Text>
              </TabPanel>
            </TabPanels>
          </Tabs>

        </Flex>
        <Box>
          <Divider pt='20px' mb='15px' color='gray' w='90%' />
        </Box>
        <Form onSubmit={submitHandler}>
          <Flex
            pt='15px'
            pl={{ base: '20px', lg: '50px' }}
            direction='column'
          >
            <Text fontWeight='bold' >
              Please enter your opinion
            </Text>
            <Flex gap={3}>
              <RadioGroup mt='15px' onChange={setRating} value={rating}>
                <Stack direction='row' color='orange.400'>
                  <Radio value='1' >
                    1<Icon as={BsStarFill} ml='5px' />
                  </Radio>
                  <Radio value='2'>
                    2<Icon as={BsStarFill} ml='5px' />
                  </Radio>
                  <Radio value='3'>
                    3<Icon as={BsStarFill} ml='5px' />
                  </Radio>
                  <Radio value='4'>
                    4<Icon as={BsStarFill} ml='5px' />
                  </Radio>
                  <Radio value='5'>
                    5<Icon as={BsStarFill} ml='5px' />
                  </Radio>
                </Stack>
              </RadioGroup>
              {/* แบบเก่า
              <Button
                borderRadius='full'
                mt='15px'
                color='orange.400'
                onClick={() => setRating(1)}
              >
                1<Icon as={BsStarFill} ml='10px' />
              </Button> */}
            </Flex>
            <Input
              mt='15px'
              mb='15px'
              placeholder='Please enter your name..'
              w={{ base: '80%', md: '60%', lg: '50%' }}
              ref={reviewUser}
              required
            />
            <Textarea
              placeholder='Please review message..'
              w={{ base: '80%', md: '60%', lg: '50%' }}
              mb='15px'
              ref={reviewMsg}
              required
            />
            <Button type='submit' w='100px' colorScheme='green' >
              Submit
            </Button>
          </Flex>
        </Form>
      </Box>
      <Flex direction='column' p={{ base: '20px', md: '0px' }}>
        <Box>
          <Divider pt='20px' mb='15px' color='gray' w='90%' />
        </Box>
        <Text fontWeight='bold' fontSize='25px' pl={{ base: '0px', lg: '50px' }} mb='15px'>
          Products you may like
        </Text>
        <ProductsList data={relatedProducts} />
      </Flex>
    </Box>
  )
}

export default ProductDetails