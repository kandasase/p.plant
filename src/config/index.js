// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFunctions, connectFunctionsEmulator } from 'firebase/functions';
import { getAuth, connectAuthEmulator } from 'firebase/auth';
import { getFirestore, connectFirestoreEmulator } from 'firebase/firestore';
import { getStorage, connectStorageEmulator } from 'firebase/storage';

const config = {
  apiKey: "AIzaSyB2fscNbaO0wA1ZFboH8DvBzU0dyhxNN5A",
  authDomain: "plants-ea02c.firebaseapp.com",
  projectId: "plants-ea02c",
  storageBucket: "plants-ea02c.appspot.com",
  messagingSenderId: "1047754948202",
  appId: "1:1047754948202:web:2deddf538c85a2399b9413",
  measurementId: "G-9SETJRQ4MS"
};

initializeApp(config);

const functions = getFunctions();
const auth = getAuth();
const db = getFirestore();
const storage = getStorage();

// ตัว emulators ถ้าจะใช้ก็มาเปิด
// if (window.location.hostname === 'localhost') {
//   connectFunctionsEmulator(functions, 'localhost', 5001);
//   connectAuthEmulator(auth, 'http://localhost:9099');
//   connectFirestoreEmulator(db, 'localhost', 8081);
//   connectStorageEmulator(storage, 'localhost', 9199);
// }

export { functions, auth, db, storage };
